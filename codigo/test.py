print ("Hola gitlab")
print ("Archivo modificado")
from tkinter import *
from pyfirmata import Arduino, util
from time import sleep
x=0
placa = Arduino ('COM5')
it = util.Iterator(placa)
#inicio el iteratodr
it.start()
pot_1= placa.get_pin('a:0:i') 
pot_2= placa.get_pin('a:1:i') 
ventana = Tk()
ventana.state('zoomed') #iniciar la apantalla maximizada
ventana.geometry('1200x800')
ventana.configure(bg = 'blue',cursor="boat")
ventana.config(relief="groove")
ventana.title("Label de datos en sensor") 
y=pot_2.read()
marco=Frame()
#marco.pack(fill="both",expand="true")
marco.pack()
marco.place(x=400,y=100)
marco.configure(bg='red',relief="groove",bd=40,width="500",height="500",cursor="man")
texto = Label(ventana, text="Interfaz de datos", bg='gold', font=("Arial Nova Light", 14), fg="white")
texto.place(x=500, y=300) 
puerto= Label(ventana,text="Datos en el pin Analogo 0",bg='cyan',font=("times new roman",14),fg='black')
puerto.place(x=500, y=400)


def update_label():
    x=pot_1.read()
    if x<0.3:
        texto.configure(bg='maroon4',fg='yellow')
        puerto.configure (text="valor ADC menor a 0.3 ",bg="white")
        texto['text']=str(x)
    if x>=0.3 and x<=0.6:
        texto.configure(bg='cadet blue1',fg='green')
        puerto.configure (text="valor ADC entre 0.3 y 0.6 ",bg="yellow")
        texto['text']=str(x)
    if x>0.6:
        texto.configure(bg='maroon4',fg='yellow')
        puerto.configure (text="valor ADC mayor a 0.6 ",bg="cyan")
        texto['text']=str(x)
    ventana.after(500,update_label)
update_label()
ventana.mainloop()